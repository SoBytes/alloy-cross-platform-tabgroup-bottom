///////////////////////////////////////////////////////////////////////////////
//
// Global collections
//
///////////////////////////////////////////////////////////////////////////////

Alloy.Collections.faker = Alloy.createCollection('faker');

///////////////////////////////////////////////////////////////////////////////
//
// Constants
//
///////////////////////////////////////////////////////////////////////////////

// properties
Alloy.Globals.PROPERTY_ENABLE_MOTION_ANIMATION = 'PROPERTY_ENABLE_MOTION_ANIMATION';
Alloy.Globals.PROPERTY_ENABLE_LIST_ANIMATION = 'PROPERTY_ENABLE_LIST_ANIMATION';

// set default properties
if (!Ti.App.Properties.hasProperty(Alloy.Globals.PROPERTY_ENABLE_MOTION_ANIMATION)) {
	Ti.App.Properties.setBool(Alloy.Globals.PROPERTY_ENABLE_MOTION_ANIMATION, true);
}
if (!Ti.App.Properties.hasProperty(Alloy.Globals.PROPERTY_ENABLE_LIST_ANIMATION)) {
	Ti.App.Properties.setBool(Alloy.Globals.PROPERTY_ENABLE_LIST_ANIMATION, true);
};

// events
Alloy.Globals.EVENT_PROPERTY_ENABLE_MOTION_ANIMATION_DID_CHANGE = 'EVENT_PROPERTY_ENABLE_MOTION_ANIMATION_DID_CHANGE';
Alloy.Globals.EVENT_PROPERTY_ENABLE_LIST_ANIMATION_DID_CHANGE = 'EVENT_PROPERTY_ENABLE_LIST_ANIMATION_DID_CHANGE';

///////////////////////////////////////////////////////////////////////////////
//
// Navigation singleton
//
///////////////////////////////////////////////////////////////////////////////

/**
 * The navigator object which handles all navigation
 * @type {Object}
 */
Alloy.Globals.Navigator = [{
	title : "My Tracking",
	focus : true,
	color : "#e4763f",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "tracking"
}, {
	title : "Health Calculators",
	color : "#c95d6e",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "health"
}, {
	title : "My Lifestyle",
	color : "#6b9205",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "lifestyle"
}, {
	title : "My Appointments",
	color : "#077b8b",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "appointments"
}];

/**
 * Init navigation
 * Called from index controller once intro animation is complete
 */
Alloy.Globals.initNavigation = function(tabgroup) {
	
	var setupTabs = Alloy.Globals.Navigator;
	var numberOfTabs = 4;
	var tabsWidths = Alloy.Globals.Device.width / numberOfTabs;

	for ( i = 0; i < setupTabs.length; i++) {

		var view = Titanium.UI.createView({
			backgroundColor : setupTabs[i].color,
			bgcolor : setupTabs[i].color,
			index : (i + 1),
			title : setupTabs[i].title,
			left : 0,
			height : "50dp",
			width : tabsWidths,
			clipMode : (OS_ANDROID) ? null : Titanium.UI.iOS.CLIP_MODE_DISABLED,
			zIndex : 1
		});

		view.add(Ti.UI.createImageView({
			touchEnabled : false,
			height : "30dp",
			width : Ti.UI.SIZE,
			image : setupTabs[i].tab_bg_image,
		}));

		tabgroup.add(view);

	}
};

///////////////////////////////////////////////////////////////////////////////
//
// Device singleton
//
///////////////////////////////////////////////////////////////////////////////

/**
 * Device information, some come from the Ti API calls and can be referenced
 * from here so multiple bridge calls aren't necessary, others generated here
 * for ease of calculations and such.
 *
 * @type {Object}
 * @param {String} version The version of the OS
 * @param {Number} versionMajor The major version of the OS
 * @param {Number} versionMinor The minor version of the OS
 * @param {Number} width The width of the device screen
 * @param {Number} height The height of the device screen
 * @param {Number} dpi The DPI of the device screen
 * @param {String} orientation The device orientation, either "landscape" or "portrait"
 * @param {String} statusBarOrientation A Ti.UI orientation value
 */
Alloy.Globals.Device = {
	version : Ti.Platform.version,
	versionMajor : parseInt(Ti.Platform.version.split(".")[0], 10),
	versionMinor : parseInt(Ti.Platform.version.split(".")[1], 10),
	width : (Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight) ? Ti.Platform.displayCaps.platformHeight : Ti.Platform.displayCaps.platformWidth,
	height : (Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight) ? Ti.Platform.displayCaps.platformWidth : Ti.Platform.displayCaps.platformHeight,
	dpi : Ti.Platform.displayCaps.dpi,
	orientation : Ti.Gesture.orientation == Ti.UI.LANDSCAPE_LEFT || Ti.Gesture.orientation == Ti.UI.LANDSCAPE_RIGHT ? "landscape" : "portrait"
};

if (OS_ANDROID) {
	Alloy.Globals.Device.width = (Alloy.Globals.Device.width / (Alloy.Globals.Device.dpi / 160));
	Alloy.Globals.Device.height = (Alloy.Globals.Device.height / (Alloy.Globals.Device.dpi / 160));
}

/*
 * Setup some padding
 */
Alloy.Globals.Device.padded = Alloy.Globals.Device.width-40;

Alloy.Globals.dpToPx = function(dp) {
	return dp * (Ti.Platform.displayCaps.platformHeight / Alloy.Globals.Device.height);
};

Alloy.Globals.pxToDp = function(px) {
	return px * (Alloy.Globals.Device.height / Ti.Platform.displayCaps.platformHeight);
}; 