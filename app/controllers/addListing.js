var args = arguments[0] || {};

/*
 * Add a listing image
 */
function addListingImage() {

	if ($.listingTitle.value == "") {
		alert("Please add listing name.");
		return true;
	}

	if ($.listingText.value == "") {
		alert("Please add listing text.");
		return true;
	}

	Titanium.Media.openPhotoGallery({
		success : function(event) {

			// Crop the image
			var img = event.media.imageAsThumbnail(Alloy.Globals.Device.width);

			var fakeModel = Alloy.createModel('faker', {
				title : $.listingTitle.value,
				text : $.listingText.value,
				image : img
			});

			fakeModel.save();
			
			$.addListing.close();

		}
	});
}