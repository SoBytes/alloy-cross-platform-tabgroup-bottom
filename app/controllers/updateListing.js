var args = arguments[0] || {};

/*
 * Setup the view
 */
$.listingTitle.value = args.title;
$.listingText.value = args.text;
$.listingImageHolder.image = args.image;

/*
 * Update a listing image
 */
function updateListing() {

	console.log("Update listing");
	var fakeCollection = Alloy.Collections.faker;

	var model = Alloy.createModel('faker');

	model.save({
		listing_id : args.id,
		title : $.listingTitle.value,
		text : $.listingText.value,
		image : $.listingImageHolder.image
	});

	fakeCollection.fetch();

	$.updateListing.close();
}

/*
 * Update a listing image
 */
function updateListingImage() {

	Titanium.Media.openPhotoGallery({
		success : function(event) {

			// Crop the image
			var img = event.media.imageAsThumbnail(Alloy.Globals.Device.width);

			$.listingImageHolder.image = img;

		}
	});
}