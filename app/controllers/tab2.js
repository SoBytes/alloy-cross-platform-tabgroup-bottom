var args = arguments[0] || {};

function openBlueWindow(e) {
	var win3 = Alloy.createController('bluewin').getView();
	$.tab2.openWindow(win3);
}

/*
 * Setup tabs
 */
Alloy.Globals.initNavigation($.tabgroup);

/*
 * Runs on window open
 */
function openWindow() {

	// Simple line animation
	$.line.animate(Ti.UI.createAnimation({
		width : Alloy.Globals.Device.width,
		opacity : 1,
		duration : 300
	}), function() {

	});

	// additions to the file
	$.tabgroup.addEventListener('click', function(_event) {
		Alloy.createController('tab' + _event.source.index).getView();
	});

}

$.tab2.addEventListener('open', openWindow);

/*
 * Clean up
 */
$.cleanup = function cleanup() {
	$.destroy();
	$.off();
};

$.tab2.addEventListener('close', $.cleanup);

$.tab2.open({
	animated : false
});
