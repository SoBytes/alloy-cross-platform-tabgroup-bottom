/*
 * Setup the tabs groups
 */
var setupTabs = [{
	title : "My Tracking",
	focus : true,
	color : "#e4763f",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "tracking"
}, {
	title : "Health Calculators",
	color : "#c95d6e",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "health"
}, {
	title : "My Lifestyle",
	color : "#6b9205",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "lifestyle"
}, {
	title : "My Appointments",
	color : "#077b8b",
	tab_bg_image : "/images/tab1_bg@2x.png",
	tab_active_image : "/images/tab1_bg@2x.png",
	open_window : "appointments"
}];

var numberOfTabs = 4;
var tabsWidths = Alloy.Globals.Device.height / numberOfTabs;

for ( i = 0; i < setupTabs.length; i++) {

	var view = Titanium.UI.createView({
		backgroundColor : setupTabs[i].color,
		bgcolor : setupTabs[i].color,
		index : (i + 1),
		title : setupTabs[i].title,
		top : 0,
		left : 0,
		height : tabsWidths,
		width : Alloy.Globals.Device.width,
		zIndex : 1
	});

	view.add(Ti.UI.createImageView({
		touchEnabled : false,
		height : "30dp",
		width : Ti.UI.SIZE,
		image : setupTabs[i].tab_bg_image,
	}));

	$.tabgroup.add(view);

}

function openWindow() {
	// additions to the file
	$.tabgroup.addEventListener('click', function(_event) {
		Alloy.createController('tab' + _event.source.index).getView();
	});
}

$.index.open(); 