var args = arguments[0] || {};

function addListing(e) {
	var addListing = Alloy.createController('addListing').getView();
	$.tab1.openWindow(addListing);
}

/*
 * Setup tabs
 */
Alloy.Globals.initNavigation($.tabgroup);

/*
 * Runs on table update
 */
function updateListing(_event) {

	console.log("Update the listing...");

	var fakeCollection = Alloy.Collections.faker;

	var model = Alloy.createModel('faker', {
		"listing_id" : _event.rowData.listing_id
	});

	var dataObj = fakeCollection.get(model);
	var passData = {
		id : dataObj.get('listing_id'),
		title : dataObj.get('title'),
		text : dataObj.get('text'),
		image : dataObj.get('image')
	};
	var updateListing = Alloy.createController('updateListing', passData).getView();
	$.tab1.openWindow(updateListing);

}

/*
 * Runs on table delet
 */
function deleteListing(_event) {

	console.log("Delete a listing...");

	var fakeCollection = Alloy.Collections.faker;

	var model = Alloy.createModel('faker', {
		"listing_id" : _event.rowData.listing_id
	});

	fakeCollection.remove(model);

	fakeCollection.sync('delete', model, function(resp) {
		alert(resp);
	});

}

$.listingTable.addEventListener('delete', deleteListing);

/*
 * Runs on window focus
 */
function focusWindow() {
    
    console.log("Fetching a listing...");
    
	//Populate the view
	var fakeCollection = Alloy.Collections.faker;
	fakeCollection.fetch();

}

$.tab1.addEventListener('focus', focusWindow);

/*
 * Runs on window open
 */
function openWindow() {

	// Simple line animation
	$.line.animate(Ti.UI.createAnimation({
		width : Alloy.Globals.Device.width,
		opacity : 1,
		duration : 300
	}), function() {

	});

	// additions to the file
	$.tabgroup.addEventListener('click', function(_event) {
		Alloy.createController('tab' + _event.source.index).getView();
	});

	//Populate the view
	var fakeCollection = Alloy.Collections.faker;
	fakeCollection.fetch();

}

$.tab1.addEventListener('open', openWindow);

/*
 * Clean up
 */
$.cleanup = function cleanup() {
	$.destroy();
	$.off();
};

$.tab1.addEventListener('close', $.cleanup);

/*
 * Open window animate false needed for Android
 */
$.tab1.open({
	animated : false
});
